/**
 * Created by Admin on 18.05.2017.
 */

import org.junit.Test

import static java.util.Objects.hash;

class TestTest extends GroovyTestCase {

    @Test
    public void testAccount(){

            Bank bk = new Bank();
            Persons p=new Persons(1,"Dan","Observator","11.11.1996");
            HashMap<Integer,ArrayList<Account>> hm;
            Account ac = new SavingAccount(1);
            bk.addAccount(ac,p);
            hm=bk.getAccounts();
            int index=hash(p.hashCode());

            for(Account a:hm.get(index)) {
                assert a.getNumber() == 1;
            }
    }
    @Test
    public void testDeposit(){
        Bank bank=new Bank();
        Account ac=new SpendingAccount(1);
        ac.deposit(1000);

        assert ac.getSum()==1000;
    }

    @Test
    public void testWithdraw(){
        Bank bank=new Bank();
        Account ac=new SpendingAccount(1);
        ac.deposit(2000);
        ac.withdraw(1000);

        assert ac.getSum()==1000;
    }
}
