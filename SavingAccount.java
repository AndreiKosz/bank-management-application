import java.io.Serializable;

/**
 * Created by Admin on 15.05.2017.
 */
public class SavingAccount extends Account implements Serializable {

    private int number;
    private double sum;
    private int persId;
    public SavingAccount(int number){
        super(number);
    }

    public SavingAccount(int number,double sum,int persId){
        super(number,sum,persId);

    }

    public void withdraw(double sum){
        if(this.getSum()!=sum) {
            throw new IllegalArgumentException("SavingAccount:You must withdraw the same sum that is deposited");
        }
        this.sum = this.getSum() - sum;
        this.setSum(this.sum);
        setChanged();
        notifyObservers();
    }

    public void deposit(double sum){
        if(this.getSum()!=0){
            throw new IllegalArgumentException("SavingAccount:You can deposit only one time");
        }
        this.sum=sum+24*0.1*sum;
        this.setSum(this.sum);
        setChanged();
        notifyObservers();

    }

}
