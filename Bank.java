import java.io.Serializable;
import java.util.*;

import static java.sql.Types.NULL;
import static java.util.Objects.hash;


public class Bank implements BankProc,Serializable {
    private HashMap<Integer,ArrayList<Account>> accounts;

    private HashSet<Persons> persons;
    public Bank(){

         accounts=new HashMap<>();
         persons=new HashSet<>();

    }


    public HashMap<Integer, ArrayList<Account>> getAccounts() {
        return accounts;
    }

    public HashSet<Persons> getPersonses() {
        return persons;
    }

    @Override
    public void addAccount(Account a, Persons p) {
        assert p!=null :"Sum is less than 0";
        int index=hash(p.hashCode());
        ArrayList<Account>accountList=new ArrayList<>();

        if(accounts.get(index)==null){
            accounts.put(index,accountList);
            accounts.get(index).add(a);
            a.addObserver(p);
        }
        else {
            accounts.get(index).add(a);
            a.addObserver(p);
        }
       assert accounts.size()!=0:"Element was not added";
       assert isWellFormed():"Creation error";

    }

    @Override
    public void editAccount(int a, Persons p,int number,double sum) {
        int index=hash(p.hashCode());
        boolean elementFound=false;
        assert sum>=0:"EditAccount:sum is less than 0";
        for(Account i:accounts.get(index)){
            if(i.getNumber()==a){
                i.setSum(sum);
                i.setNumber(number);
                elementFound=true;
            }

        }
        assert elementFound:"Account not found";

    }

    @Override
    public void deleteAccount(int a, int hashKey) {
        int index=hash(hashKey);
        int cont=0;
        boolean elementFound=false;
        assert hashKey>0 :"DeleteAccount:given hashKey is invalid";
        for(Account i:accounts.get(index)){

            if(i.getNumber()==a){

                accounts.get(index).remove(cont);
                elementFound=true;
            }
            cont++;

        }
        assert elementFound:"Account not found";
        assert isWellFormed():"Creation error after delete";

    }


    @Override
    public void addPersons(Persons p) {
        assert p!=null:"Persons that is added is null";
        persons.add(p);
        assert isWellFormed2();

    }

    @Override
    public void editPersons(int id,String name,String birthDay,String address ) {
        boolean elementFound=false;
        assert id>0:"Invalid id";
        for(Persons i:persons){
            if(i.getId()==id){
                i.setName(name);
                i.setAddress(address);
                i.setBirthDay(birthDay);
                elementFound=true;
            }
        }
        assert elementFound:"Person not found";
    }

    @Override
    public void deletePersons(int id) {
        boolean elementFound=false;
        assert id>0:"DeletePerson:invalid id";
        for(Persons i:persons){
            if(i.getId()==id){
                persons.remove(i);
                elementFound=true;
            }
        }
        assert elementFound:"Person not found";
        assert isWellFormed2():"Error at persons after delete";

    }

    private boolean isWellFormed() {
        assert !accounts.isEmpty(): "Nu exista baza de date la banca";
        boolean rezultat = true;
        int i = 0;
        Iterator it=accounts.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair=(Map.Entry)it.next();
            i++;
            if (pair.getValue() == null) {
                rezultat = false;
            }
        }
        if (accounts.size() != i) {
            rezultat = false;
        }
        return rezultat;
    }

    private boolean isWellFormed2() {
        assert !persons.isEmpty(): "No persons available";
        boolean rezultat = true;
        int i = 0;
        for(Persons p:persons){
            i++;
            if(p==null)
                rezultat=false;
        }

        if (persons.size() != i) {
            rezultat = false;
        }
        return rezultat;
    }


}
