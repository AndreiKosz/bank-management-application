import java.util.ArrayList;

import static java.util.Objects.hash;

/**
 * Created by Admin on 16.05.2017.
 */
public interface BankProc {

    public void addAccount(Account a, Persons p);
    public void editAccount(int a,Persons p,int number,double sum);
    public void deleteAccount(int a,int hashKey);

    public void addPersons(Persons p);
    public void editPersons(int id,String name,String birthDay,String address);
    public void deletePersons(int id);

}
