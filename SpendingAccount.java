import javafx.beans.property.StringProperty;

import java.io.Serializable;

/**
 * Created by Admin on 15.05.2017.
 */
public class SpendingAccount extends Account implements Serializable {

    private double sum;
    private int number;
    public SpendingAccount(int number){
        super(number);
    }

    public void withdraw(double sum){
        if(this.getSum()<sum){
           throw new IllegalArgumentException("SpendingAccount:The sum that is deposited is less than the sum you want to withdraw");

        }
        this.sum=this.getSum()-sum;
        this.setSum(this.sum);
        setChanged();
        notifyObservers();

    }

    public void deposit(double sum){
        this.sum=this.getSum()+sum;
        this.setSum(this.sum);
        setChanged();
       notifyObservers();
    }

}
