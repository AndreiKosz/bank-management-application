import com.sun.xml.internal.bind.v2.util.QNameMap;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;

import java.io.Serializable;
import java.util.*;
import java.util.Map.Entry;

import java.awt.*;
import java.io.IOException;
import java.net.URL;

import static java.util.Objects.hash;

/**
 * Created by Admin on 17.05.2017.
 */
public class Controller implements Serializable {
    private FirstWindowController controller=new FirstWindowController();
    private Serialization ser=new Serialization();

    @FXML
    // The reference of inputText will be injected by the FXML loader
    private TextField persIdAcc,savingAccount,spendingAccount,accNumber,persIdAccEdit,editNumber,editSum,accDeposit,depositSum,accWithdraw,withdrawSum,depositPersId,withdrawPersId,idPers,persName,persAddress,persBirthDay,persEdit,persIdEdit,persNameEdit,persAddressEdit,persBirthDayEdit;


    // The reference of outputText will be injected by the FXML loader
    @FXML
    private TextArea log;
    @FXML
    private Label text1;

    // location and resources will be automatically injected by the FXML loader
    @FXML
    private URL location;

    @FXML
    private ResourceBundle resources;
    @FXML
    private TableView<SavingAccount> accountTable;
    @FXML
    private TableView<Persons> personTable;

    @FXML
    private TableColumn<SavingAccount,Integer> numberColumn;
    @FXML
    private TableColumn<SavingAccount,Integer> sumColumn;
    @FXML
    private TableColumn<SavingAccount,Integer> persColumn;

    @FXML
    private TableColumn<Persons,Integer> columnId;
    @FXML
    private TableColumn<Persons,String> columnName;
    @FXML
    private TableColumn<Persons,String> columnAddress;
    @FXML
    private TableColumn<Persons,String> columnBirthDay;


    public void openAccountWindow()throws IOException {
        controller.showAccountWindow();
    }

    public void openPersonsWindow() throws IOException{
        controller.showPersonsWindow();
    }

    public void addSavingAccount(){
        Persons p=null;
        int personId=Integer.parseInt(persIdAcc.getText());
        int accNumber=Integer.parseInt(savingAccount.getText());
        Account ac=new SavingAccount(accNumber);
        Bank bank=ser.deserialize();
        for(Persons i:bank.getPersonses()){
            if(i.getId()==personId) {
                p = i;

            }
        }
        bank.addAccount(ac,p);
        //if(p!=null)
        //ac.addObserver(p);
        ser.serialize(bank);

    }

    public void addSpendingAccount(){
        Persons p=null;
        int personId=Integer.parseInt(persIdAcc.getText());
        int accNumber=Integer.parseInt(spendingAccount.getText());
        Account ac=new SpendingAccount(accNumber);
        Bank bank=ser.deserialize();
        for(Persons i:bank.getPersonses()){
            if(i.getId()==personId) {
                p = i;
            }
        }
        bank.addAccount(ac,p);
       // if(p!=null)
            //ac.addObserver(p);
        ser.serialize(bank);

    }

    public void editAccount(){
        Persons p=null;
        int personId=Integer.parseInt(persIdAccEdit.getText());
        int accNumber1=Integer.parseInt(accNumber.getText());
        int editedNumber=Integer.parseInt(editNumber.getText());
        double editedSum=Double.parseDouble(editSum.getText());
        Bank bank=ser.deserialize();
        for(Persons i:bank.getPersonses()){
            if(i.getId()==personId){
                p=i;

            }
        }
        bank.editAccount(accNumber1,p,editedNumber,editedSum);
        ser.serialize(bank);

    }

    public void deposit(){
        int personId=Integer.parseInt(depositPersId.getText());
        int accNumber1=Integer.parseInt(accDeposit.getText());
        double sum=Double.parseDouble(depositSum.getText());
        int index=-1;
        HashMap<Integer,ArrayList<Account>> hm;
        Bank bank=ser.deserialize();
        hm=bank.getAccounts();

        for(Persons p:bank.getPersonses()){
            if(p.getId()==personId){
                index=hash(p.hashCode());
                for(Account a:hm.get(index)){
                    if(a.getNumber()==accNumber1){
                        a.deposit(sum);
                    }
                }

            }
        }
        ser.serialize(bank);
    }

    public void withdraw(){
        int personId=Integer.parseInt(withdrawPersId.getText());
        int accNumber1=Integer.parseInt(accWithdraw.getText());
        double sum=Double.parseDouble(withdrawSum.getText());
        int index=-1;
        HashMap<Integer,ArrayList<Account>> hm;
        Bank bank=ser.deserialize();
        hm=bank.getAccounts();

        for(Persons p:bank.getPersonses()){
            if(p.getId()==personId){
                index=hash(p.hashCode());
                for(Account a:hm.get(index)){
                    if(a.getNumber()==accNumber1){
                        a.withdraw(sum);
                    }
                }

            }
        }
        ser.serialize(bank);
    }

    public void accountTableView(){

       //TableColumn<SavingAccount,Integer> numberColumn=new TableColumn<>("Number");
        numberColumn.setCellValueFactory(new PropertyValueFactory<SavingAccount, Integer>("number"));

        //TableColumn<SavingAccount,Integer> sumColumn=new TableColumn<>("Sum");
        sumColumn.setCellValueFactory(new PropertyValueFactory<SavingAccount, Integer>("sum"));

       // TableColumn<SavingAccount,Integer> personColumn=new TableColumn<>("PersonId");
        persColumn.setCellValueFactory(new PropertyValueFactory<SavingAccount, Integer>("persId"));

        accountTable.setItems(null);
        accountTable.setItems(getAccounts());
        //accountTable.getColumns().addAll(numberColumn,sumColumn);

    }

    public ObservableList<SavingAccount> getAccounts(){

        Bank bank;
        int personKey=0;
        HashSet<Persons>pers;
        ObservableList<SavingAccount> accounts= FXCollections.observableArrayList();
        bank=ser.deserialize();
        pers=bank.getPersonses();
        for(Entry<Integer,ArrayList<Account>> i:bank.getAccounts().entrySet()){
            for(Account a:i.getValue()){
                for(Persons p:pers){
                    if(hash(p.hashCode())==i.getKey()){
                        personKey=p.getId();

                    }
                }
                accounts.add(new SavingAccount(a.getNumber(),a.getSum(),personKey));
            }

        }

        return accounts;
    }

    public void delete(){
        ObservableList<SavingAccount>accountSelected,allAccounts;
        Bank bank;
        int personKey=0;
        HashSet<Persons>pers;
        bank=ser.deserialize();
        pers=bank.getPersonses();
        allAccounts=accountTable.getItems();
        accountSelected=accountTable.getSelectionModel().getSelectedItems();


        bank.deleteAccount(accountSelected.get(0).getNumber(),accountSelected.get(0).getPersId());
        ser.serialize(bank);

        accountSelected.forEach(allAccounts::remove);

    }

    public void addPerson(){

        int personId=Integer.parseInt(idPers.getText());
        Persons p=new Persons(personId,persName.getText(),persBirthDay.getText(),persAddress.getText());
        Bank bank=ser.deserialize();
        bank.addPersons(p);
        ser.serialize(bank);

    }

    public void editPerson(){
        int personId=Integer.parseInt(persEdit.getText());
        int editedId=Integer.parseInt(persIdEdit.getText());
        HashSet<Persons>pers;
        Bank bank;
        bank=ser.deserialize();

        pers=bank.getPersonses();

        for(Persons p:pers){
            if(p.getId()==personId){
                bank.editPersons(editedId,persNameEdit.getText(),persBirthDayEdit.getText(),persAddressEdit.getText());
                ser.serialize(bank);
            }
        }

    }

    public ObservableList<Persons> getPersons(){

        Bank bank;
        HashSet<Persons>pers;
        ObservableList<Persons> persons= FXCollections.observableArrayList();
        bank=ser.deserialize();
        pers=bank.getPersonses();
        for(Persons i:pers){

                persons.add(new Persons(i.getId(),i.getName(),i.getBirthDay(),i.getAddress()));

        }

        return persons;
    }

    public void personsTableView(){

        //TableColumn<SavingAccount,Integer> numberColumn=new TableColumn<>("Number");
        columnId.setCellValueFactory(new PropertyValueFactory<Persons, Integer>("id"));

        columnName.setCellValueFactory(new PropertyValueFactory<Persons, String>("name"));

        //TableColumn<SavingAccount,Integer> sumColumn=new TableColumn<>("Sum");
        columnBirthDay.setCellValueFactory(new PropertyValueFactory<Persons, String>("birthDay"));

        // TableColumn<SavingAccount,Integer> personColumn=new TableColumn<>("PersonId");
        columnAddress.setCellValueFactory(new PropertyValueFactory<Persons, String>("address"));

        personTable.setItems(null);
        personTable.setItems(getPersons());
        //accountTable.getColumns().addAll(numberColumn,sumColumn);

    }

    public void deletePerson(){
        ObservableList<Persons>personsSelected,allPersons;
        Bank bank;
        HashSet<Persons>pers;
        bank=ser.deserialize();
        pers=bank.getPersonses();
        allPersons=personTable.getItems();
        personsSelected=personTable.getSelectionModel().getSelectedItems();


        bank.deletePersons(personsSelected.get(0).getId());
        ser.serialize(bank);

        personsSelected.forEach(allPersons::remove);

    }


}
