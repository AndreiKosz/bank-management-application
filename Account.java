import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;

import java.io.Serializable;
import java.util.Observable;

/**
 * Created by Admin on 15.05.2017.
 */
public abstract class Account extends Observable implements Serializable {

    private double sum;
    private int number;
    private int persId;

    public Account(int number){
        this.sum=0.0;
        this.number=number;

    }

    public Account(int number,double sum,int persId){
        this.sum=sum;
        this.number=number;
        this.persId=persId;
    }

    public abstract void withdraw(double sum);
    public abstract void deposit(double sum);

    public int getPersId() {
        return persId;
    }

    public void setPersId(int persId) {
        this.persId = persId;
    }

    public double getSum() {
        return sum;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public void setSum(double sum) {
        this.sum = sum;
    }
}

