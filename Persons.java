import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import java.io.Serializable;
import java.util.Observable;
import java.util.Observer;

/**
 * Created by Admin on 15.05.2017.
 */
public class Persons implements Serializable,Observer {

    private int id;
    private String name;
    private String birthDay;
    private String address;
    private Account account;

    public Persons(int id,String name,String birthDay,String address){
        this.id=id;
        this.name=name;
        this.birthDay=birthDay;
        this.address=address;

    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public String getBirthDay() {
        return birthDay;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setBirthDay(String birthDay) {
        this.birthDay = birthDay;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int hashCode(){
        return id;
    }

    public boolean equals(Object obj){
        if(obj==null)
            return false;
        if(!(obj instanceof Persons))
            return false;

        return this.getId()==((Persons) obj).getId();


    }

    @Override
    public void update(Observable o, Object arg) {
        account=(Account)o;
        System.out.println(account.getClass()+" of person "+this.getName()+" has changed");

    }
}
