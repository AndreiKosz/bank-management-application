import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Admin on 17.05.2017.
 */
public class Serialization {

    public void serialize(Bank bankAccounts){
        try{
            FileOutputStream fileOut=new FileOutputStream("E:/Faculta/Bank/src/Bank.ser");
            ObjectOutputStream out=new ObjectOutputStream(fileOut);
            out.writeObject(bankAccounts);
            out.close();
            fileOut.close();

        }catch (IOException e){
            e.printStackTrace();

        }

    }

public Bank deserialize(){
    Bank bank=null;
    try{
        FileInputStream fileIn =new FileInputStream("E:/Faculta/Bank/src/Bank.ser");
        ObjectInputStream in = new ObjectInputStream(fileIn);
        bank=(Bank)in.readObject();
    }
    catch(IOException|ClassNotFoundException e){
        e.printStackTrace();
    }

    return bank;
}
}
